#!/usr/bin/env python3

import time
import sys
import csv
import os
import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from pathlib import Path

# Assume the script is run from automation directory
p = Path().cwd()
runs = p / "runs"
runs.mkdir(exist_ok=True)
dirname = runs / time.strftime("%Y%m%d%H%M%S")
dirname.mkdir(exist_ok=True)

template = p.parent / "template" / "PSO_NL_Template.nlogo"
assert template.exists()

url = "http://www.netlogoweb.org/launch#Load"

options = Options()
options.add_argument('window-size=1920x1080')
options.add_argument("--incognito")
options.add_argument("--headless")  # If you want to see the browser, comment this
prefs = {'download.default_directory': str(dirname),
                     'download.prompt_for_download': False,
                     'download.directory_upgrade': True,
                     'safebrowsing.enabled': False,
                     'safebrowsing.disable_download_protection': True}
options.add_experimental_option('prefs', prefs)

# Read output file in reverse to increase speed
# Source: https://stackoverflow.com/questions/2301789/how-to-read-a-file-in-reverse-order


def reverse_readline(filename, buf_size=8192):
    """A generator that returns the lines of a file in reverse order"""
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            # The first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # If the previous chunk starts right from the beginning of line
                # do not concat the segment to the last line of new chunk.
                # Instead, yield the segment first
                if buffer[-1] != '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if lines[index]:
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment


class BaseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(options=options)
        cls.driver.get(url)
        cls.driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
        params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': str(dirname)}}
        command_result = cls.driver.execute("send_command", params)

        time.sleep(3)
        # First need to upload the filder for everything to show up
        model_upload = cls.driver.find_element(By.XPATH, '/html/body/div[3]/div/div/div[1]/div[2]/label/input')
        model_upload.send_keys(str(template.absolute()))

        # Takes a bit until we the file loads
        time.sleep(3)
        frame = cls.driver.find_element(By.XPATH, '/html/body/div[3]/div/div/div[2]/iframe')
        cls.driver.switch_to.frame(frame)

        cls.action = ActionChains(cls.driver)

        # Now we can get the elements
        cls.ticks                      = cls.driver.find_element(By.XPATH, '//*[@id="netlogo-model-container"]/div/div[2]/label/input')
        cls.population_size            = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[2]/div/span[2]/input")
        cls.particle_speed_limit       = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[7]/div/span[2]/input")
        cls.particle_inertia           = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[5]/div/span[2]/input")
        cls.personal_confidence        = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[3]/div/span[2]/input")
        cls.swarm_confidence           = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[4]/div/span[2]/input")
        cls.constraints_check          = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[10]/input")
        cls.constraint_select          = Select(cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[11]/select"))
        cls.constraint_handling_method = Select(cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[14]/select"))
        cls.fitness_function           = Select(cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[9]/select"))

        cls.setup = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/button[1]")
        cls.go    = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/label[1]")
        cls.step  = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/button[2]")
        cls.save  = cls.driver.find_element(By.XPATH, "/html/body/div[3]/div/div[2]/div[5]/button[5]")

    @classmethod
    def tearDownClass(cls):
        screenshot_name = str(dirname) + "/screenshot.png"
        cls.driver.save_screenshot(screenshot_name)
        cls.driver.implicitly_wait(2)
        cls.driver.close()

        result_file = dirname / "filename.txt"
        assert result_file.exists()

        csv_file = dirname / "fitness.csv"
        lines = reverse_readline(result_file)
        # Only two values we care about is a) iteration step b) fitness value
        # First value will always be "EXTENSIONS"
        # Last value will always be header with "pen down?"
        arr = []
        line = next(lines)
        while "pen down?" not in line:
            if "EXTENSIONS" in line:
                line = next(lines)
                continue
            it, val, _, _ = line.split(",")
            arr.append([it, val])
            line = next(lines)
        arr.reverse()  # since we read backwards

        with open(str(csv_file), "w", newline="") as f:
            csv_writer = csv.writer(f, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerows(arr)


class LangermannTest(BaseTest):
    def setUp(self):
        self.fitness_function.select_by_visible_text("Langermann")
        # self.fitness_function.select_by_visible_text("Example function")

    def test_constraint1(self):
        pop_size = "10"
        particle_inertia = "0.1"
        particle_speed_limit = "8"
        personal_confidence = "0.4"
        swarm_confidence = "1.5"

        self.action.click(self.population_size).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).send_keys(Keys.DELETE).send_keys(pop_size).perform()
        self.action.click(self.particle_inertia).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).send_keys(Keys.DELETE).send_keys(particle_inertia).perform()
        self.action.click(self.particle_speed_limit).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).send_keys(Keys.DELETE).send_keys(particle_speed_limit).perform()
        self.action.click(self.personal_confidence).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).send_keys(Keys.DELETE).send_keys(personal_confidence).perform()
        self.action.click(self.swarm_confidence).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).send_keys(Keys.DELETE).send_keys(swarm_confidence).perform()

        self.constraints_check.click()
        self.constraint_handling_method.select_by_index(1)  # penalty method
        self.constraint_select.select_by_index(1)

        self.setup.click()
        self.go.click()
        self.save.click()

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass


class SchwefelTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_constraint1(self):
        pass

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass

    def tearDown(self):
        pass


class ShubertTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_constraint1(self):
        pass

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass

    def tearDown(self):
        pass


class SchafferTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_constraint1(self):
        pass

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass

    def tearDown(self):
        pass


class EggholderTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_constraint1(self):
        pass

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass

    def tearDown(self):
        pass


class EasomTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_constraint1(self):
        pass

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass

    def tearDown(self):
        pass


class BoothTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_constraint1(self):
        pass

    def test_constraint2(self):
        pass

    def test_constraint3(self):
        pass

    def test_constraint4(self):
        pass

    def test_constraint5(self):
        pass

    def test_constraint6(self):
        pass

    def test_constraint7(self):
        pass

    def test_constraint8(self):
        pass

    def test_constraint9(self):
        pass

    def test_constraint10(self):
        pass

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
