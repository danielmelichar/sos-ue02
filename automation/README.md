# Automation Scripts

A small script for reproducible tests using [Selenium](https://www.selenium.dev/) and [Python Unittests](https://docs.python.org/3/library/unittest.html).

* For Selenium Python bindings, see [here](https://selenium-python.readthedocs.io/).
* For difference between NetLogo Web and Desktop, see [here](http://www.netlogoweb.org/docs/differences).


## Requirements

The scripts are set up to work with any Chromium Browser (i.e. Google Chrome, Brave, Microsoft Edge, etc). You only need to install the `chrome-driver` (see [this](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)) so that Selenium can interact with the web pages. You will also need Python 3.9.2 to actually run the scripts.

Example install under Debian bullseye:

```bash
$ # Core dependencies for experiment.py
$ sudo apt install -y chromium chromium-driver python3 python3-dev python3-pip
$ # Seperate dependencies from global python installation
$ python3 -m venv venv
$ # Activate venv
$ source venv/bin/activate
$ # Now install Python dependenceis
$ (venv) pip3 install -r requirements.txt
```

Now you should be able to run the script. You can define further options, see the help menu like this

```bash
$ python3 experiment.py --help
```

## Configuration and Test definition

Every test has a few parameters:

     pop_size = "100"
     particle_inertia = "0.1"
     particle_speed_limit = "8"
     personal_confidence = "0.4"
     swarm_confidence = "1.5"

And every class has one parameter that needs to be defined in its `setUp()` function

    self.fitness_function.select_by_visible_text("Langermann")

Everything else can be copied. I might move everything to a function so that only the parameters need to be defined, and a `run_tests()` will execute. See [`test_constraint1()`](https://gitlab.com/danielmelichar/sos-ue02/-/blob/develop-automation/automation/experiments.py#L143) for a working example. 

## How To Run

Simply run the script from your virtual environment.

```bash
$ (venv) python3 experiment.py
```

With the default configuration, the script will launch a Chromium instance in the background, run all tests, generate a `.csv` file including iterations and fitness, take a screenshot of the final screen, and put everything in a folder inside `run/` with a timestamp as the foldername. 

For now, **all** tests are run whenever you call `experiment.py`. In the future, we can define specific tests to run using [Testsuites](https://docs.python.org/3/library/unittest.html#grouping-tests) or by switching to [pytest](https://docs.pytest.org/en/6.2.x/).
