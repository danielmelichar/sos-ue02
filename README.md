# sos-ue02

**Deadline**: 2021-12-31

## Description

The goal of this assignment is to understand the [Particle Swarm Optimization](https://en.wikipedia.org/wiki/Particle_swarm_optimization) (PSO) algorithm by 

1. Implementing variants of the PSO algorithm with [Netlogo](https://ccl.northwestern.edu/netlogo/docs/)
2. Observing the algorithm
3. Performing experiments and analysing the results

A NetLogo template (see `./template/`) is provided for the core structure of a PSO algorithm. Our task is to modify the template's fitness function and constraint. The modifications should include a combination of three fitness functions and three constraints. During experimentation, we shall also vary the parameters (size, inertia, velocity limits, attraction factors, etc.) 

## Fitness functions

Descriptions of the functions below and further functions can be found [here](https://www.sfu.ca/~ssurjano/optimization.html).

- Langerman function 
- Schwefel function 
- Shubert function 
- Schaffer function 
- Eggholder function 
- Easom function 
- Booth's function 

## Constraints

The template implements constraint handling by _Rejection_, we shall implement a contstraint method based on the _Penalty_ method. We need to use the option _"Constraint handling method"_ to switch between constraints and the switch control _"Use constraints"_ to determine whether constraints are used or not.

- $`c1(x,y): x^2 + x^2 \lt 6000`$ 
- $`c2(x,y): x > 3y \quad \textrm{or} \quad 3x \lt y`$ 
- $`c3(x,y): x > y + 20 \quad \textrm{or} \quad x \lt y - 20`$ 
- $`c4(x,y): x^2+y^2 \lt 9000 \quad \textrm{and} \quad x^2 + y^2 > 4000`$ 
- $`c5(x,y): x \gt y`$ 
- $`c6(x,y): 10x \lt y^2`$ 
- $`c7(x,y): tan(2x) \lt tan(4y)`$ 
- $`c8(x,y): sin(8x) \lt sin(8y)`$ 
- $`c9(x,y): sin(x)*sin(y)\lt0.2`$ 
- $`c10(x,y): tan(xy) \lt 1`$ 


## Analysis

During experimentation, the following factors should be considered:

- Convergence of the algorithm regarding (i) quality of solution (ii) sticking to local minima (iii) stagnation in relation to parameters
- How functions with different topologies require different parameters
- Respecting advantages and disadvantages of rejection and penalty constraint methods
- Diverse experiments with/without constraints, different constraints, and with different fitness functions

Convergence falls in one of three categories: optimal, early, stagnation. See the `./doc/assignment.pdf` for more. 

## Report

Empirical results need to be shown with the analysis factors above. For robust results the test should be run 10 to 30 times. The report shall have a basic reporting structure:

- Abstract
- Implementation
- Experiments
- Results and analysis
- Conclusion

Overleaf is used for writing the report. Editing is possible with this [link](https://www.overleaf.com/5299814854nkhtwwcdjddm) or by invite.
